Table of Contents
-----------------
1. [Description](#description)
2. [Application](#application)
3. [Infrastructure and repo structure](#infrastructure-and-repo-structure)
4. [Pipeline](#pipeline)
5. [Deployment](#deployment)
6. [Simulation](#simulation)
7. [Issues](#issues)
8. [Disclaimer](#disclaimer)
9. [License](#license)
10. [Author](#author-information)

Description
=========

## Blue/Green Deployment Progression with Flagger (c)
[![BlueGreen](https://raw.githubusercontent.com/fluxcd/flagger/main/docs/diagrams/flagger-bluegreen-steps.png)](https://docs.flagger.app/usage/deployment-strategies)


Repo provides a sample illustration of Blue/Green deployment exclusively over kubernetes eco-system following strict GitOps model using FluxCD and flagger.

Application
=========

The application used for this case is called `rest-api-app` a RESTfull API ready custom developped app with flask frontend and mongodb backend. The source code can be found in [build repo](https://gitlab.com/dmitry_rachkov/rest-api-app/build/-/tree/master) with more details to it. Application is packaged in container and deployment release further described in a helm chart. Helm release creates deployment of flask application and mongodb taken from [bitnami official repo](https://github.com/bitnami/charts/tree/master/bitnami/mongodb) (all credits go to them).

Helm chart also deploys 2 services: 1 ClusterIP for front-end to backend communication and second one LoadBalancer -> for native EKS integration that creates classical LB and public DNS record.

Infrastructure and repo structure
------------

Infrastructure is deployed via another repository that can be found in [infra-fluxcd repo](https://gitlab.com/dmitry_rachkov/rest-api-app/infra-fluxcd/-/tree/master). In a nutshell infra comprises from AWS EKS cluster and fluxcd both bootstraped during terraform pipeline. FluxCD is targeted to write its flux-system manifests directly to this repo (one can find them within `cd/flux-system` path).

The structure of this repo the following:

```
├── README.md                                                                             
├── cd                    
│   ├── flux-system                                                                        
│   │   ├── gotk-components.yaml                                                           
│   │   ├── gotk-sync.yaml                                                                 
│   │   └── kustomization.yaml                                                             
│   └── sync                                                                               
│       ├── flagger                                                                        
│       │   ├── blueGreen.yaml                                                             
│       │   └── load-tester                                                                
│       │       ├── deployment.yaml                                                        
│       │       ├── kustomization.yaml                                                     
│       │       └── service.yaml                                                           
│       └── flux                                                                           
│           ├── helm_release.yaml                                                          
│           └── helm_source.yaml                                                           
├── flagger                       
└── rest-api-app 
```

`cd/sync` folder is used to store the manifests required for fluxcd and flagger and they are automatically reconcialiated and applied as per flux's configuration (deployed in pipeline, check below) this repository as added as source, hence any manifest stored/changed in master will be automatically applied.

`cd/sync/flux` - stores `helm_source` and `helm_release` manifests, simply put they create flux's configuration for HelmController to watch this repository `./rest-api-app` chart in particular, if chart is going to change in master branch - changes will be reconcialiated and applied automatically - this is how CD process initiated and continuesly happening via strict GitOps model.

`cd/sync/flagger` - stores flagger Canary CRD deployment manifest to describe deployment progression and Blue/Green mechanics, more about it in below Deployment section.


Pipeline
------------

A multistage gitlab-ci pipeline is built using kubernetes as executor and consists of:
- Scan stage:
  -  [kubesec-sast](https://github.com/controlplaneio/kubesec): using native gitlab-ci integration with kubesec static vulnerability scan over k8s manifests
- Flagger stage:
  - deployment of flagger with helm chart
- FluxCD stage:
  - Using flux-cli image this repository is added to `GitRepositories` for Flux's source controller to watch master branch. Once this stage is completed the manifests in cd folder are going to be automatically applied and actual deployment is going to be launched.  


Deployment
------------

The deployment pattern followed is well-explained theoretically [here](https://docs.flagger.app/tutorials/kubernetes-blue-green), simply put native kubernetes CNI mesh is used to switch the traffic. The Canary CRD of flagger provides instructions to flagger of what to do, these actions explained below.

**Blue/Green scenario:**
- on bootstrap (apply of `blueGreen.yaml`), Flagger will create three ClusterIP services (svc-primary, svc-canary, svc) and a shadow deployment named svc-primary that represents the blue version
- when a new version is detected, Flagger would scale up the green version and run the Flagger would start the load tests and validate them with custom Prometheus queries if the load test analysis is successful, Flagger will promote the new version to app-primary and scale down the green version. For load-testing an auxilarry deployment is used stored in `cd/sync/flagger/load-test`, credits go to [developer](https://github.com/fluxcd/flagger/blob/main/kustomize/tester/service.yaml)


Simulation
------------

To simulate the progression and deployment one should navigate [build repo](https://gitlab.com/dmitry_rachkov/rest-api-app/build/-/tree/master), create a dev branch followed by change of `application/static/css/main.css` file modifying the backend colour from `deepskyblue` to `lightseagreen`, string 2. Once that is done a new release tag must be created followed by successfull merge request merged to master. This will initiate the build pipeline and new artifact (new image) will be created properly tagged.

Next step is to crete a new dev branch in this repository followed by change of cd/sync/flux/helm_release.yaml file introducing new tag in line 20. Once new MR is merged to master, flux will reconcile the changed and initiate new helm release.

Since application is having an public internet endpoint over ELB, one can witness the progression via root landing web page. To navigate the public URL one can use this command over EKS cluster `kubectl get svc flask-service -o go-template --template='{{range .status.loadBalancer.ingress}}{{.hostname}}{{end}}'` that will provide ELB's public DNS record. Using this record and adding uri `/v1` once can land on the home page - this is an example `http://a93cadb523e62437fa52b3f7e2f8445e-721502365.us-east-1.elb.amazonaws.com/v1/`.


Issues
------------

Unfortunatelly current upstream version of flagger Canary manifest is having issues within this deployment scenario and application setup, as a result progression is not happening as expected (flagger stops on initialization phase), instead traditional RollingUpdate happens. This has been issued in this [bug-report](https://github.com/fluxcd/flagger/issues/948) and will be fixed asap.


Disclaimer
-------

TLS encryption for public AWS endpoint (classic LB) was not in a scope of this project. This is a mockup application that is not designed for production use. Production release must have TLS encryption over https and would have cert-manager deployment with automatic certificate generation as well as aws ingress controller instead of classic LB.

Mongodb is having authenticaiton turned off which is **NOT** how it should be run with real data in production, author's intention is to demonstrate an ability of a stack and do a mock up excercise, rather than do production-grade setup. Mongodb is also configured to be run in standalone mode, in production use case one would want to read about mongodb clusterisation and concept of [Replica Set](https://docs.mongodb.com/manual/administration/replica-set-deployment/) (not to be confused with k8s replicas).


License
-------

GNU GPL license


Author Information
------------------

Dmitry Rachkov
